import javafx.application.Application;
import javafx.collections.ListChangeListener;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Created by Khiem on 12/18/2016.
 */
public class main extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        root = new Page();
        sc = new Scene(root, 800, 600, Color.ANTIQUEWHITE);
        stage.setScene(sc);
        stage.show();
        Text text = new Text("This is a test");
        text.setX(10);
        text.setY(50);
        text.setFont(new Font(20));
        text.getTransforms().add(new Rotate(60, 60, 60));
        handle();
        root.getChildren().addListener((ListChangeListener.Change<? extends Object> change) -> {
            System.out.println("changed "+ root.getChildren().size());
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
    private void handle() {

        sc.setOnMousePressed(event -> {
            if (root.startNode != AbstractNode.NIL) {
                    root.startNode.deselect();
                    root.startNode = AbstractNode.NIL;
                }
                else if(event.isPrimaryButtonDown()) {
                    Vertex node = new Vertex(event.getX(), event.getY());
                    root.getChildren().addAll(node);
                    nodes.add(node);
                }
        });
    }

    public AbstractNode findNode(double x, double y) {
        for (AbstractNode node : nodes) {
            if (node.contains(x, y)) {
                return node;
            }
        }
        return null;
    }

    public void addEdge(AbstractNode start, AbstractNode end) {
        root.getChildren().addAll(new DirectedEdge(start, end));
        start.toFront();
        end.toFront();
    }
    private Page root;
    private Scene sc;
    private ArrayList<AbstractNode> nodes = new ArrayList<>();
    /*
    private ToolBar toolbar = new ToolBar(){
        {

            Button edge = new Button("Add edge"){
                {
                    setOnAction(event -> {
                        mode.set(MODE.EDGE);
                    });
                }
            };
            Button node = new Button("Add node"){
                {
                    setOnAction(event -> {
                        mode.set(MODE.NODE);
                    });
                }
            };
            Button vertex = new Button("Add vertex"){
            {
                setOnAction(event -> {

                });
            }
        };
            this.getItems().addAll(edge, node, vertex);
            relocate(0,0);
        }
    };
    */
}

class Page extends Group {
    protected AbstractNode startNode = AbstractNode.NIL;

}
