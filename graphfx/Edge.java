import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

/**
 * Created by Khiem on 12/19/2016.
 */
public abstract class Edge extends Group {
    private AbstractNode startNode;
    private AbstractNode endNode;
    private Label label = new Label("Hi");
    protected Line line;

    public Edge(AbstractNode startNode, AbstractNode endNode) {
        super();
        line = new Line(startNode.getLayoutX(),startNode.getLayoutY(), endNode.getLayoutX(), endNode.getLayoutY());
        line.setFill(Color.BLACK);
        line.setStroke(Color.BLACK);
        this.startNode = startNode;
        this.endNode = endNode;
        line.startXProperty().bind(startNode.connectionXProperty);
        line.startYProperty().bind(startNode.connectionYProperty);
        line.endXProperty().bind(endNode.connectionXProperty);
        line.endYProperty().bind(endNode.connectionYProperty);
        label.layoutXProperty().bind((line.startXProperty().add(line.endXProperty())).divide(2).subtract(label.widthProperty()));
        label.layoutYProperty().bind((line.startYProperty().add(line.endYProperty())).divide(2).subtract(label.heightProperty()));
        getChildren().addAll(line,label);
        handle();
    }

    private void handle() {
        setOnMousePressed(e -> {
            if (e.isSecondaryButtonDown()) {
                popup.show();
                e.consume();
            }
        });
    }

    public Edge(AbstractNode startNode, AbstractNode endNode, String string) {
        this(startNode, endNode);
        label.setText(string);
    }

    public abstract double getCost();
    private
    Stage popup = new Stage() {
        VBox y = new VBox();
        TextArea t = new TextArea();

        {
            t.minWidthProperty().bind(widthProperty());
            t.minHeightProperty().bind(heightProperty());
            setTitle("Editor");
            y.getChildren().addAll(t);
            this.setScene(new Scene(y, 200, 200));
            t.textProperty().bindBidirectional(label.textProperty());
        }
    };
}
